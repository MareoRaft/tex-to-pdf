#!/bin/bash

# ------------------------------------------------------------------------------
# Setting Bash options
# ------------------------------------------------------------------------------

# Remap STDERR's destination to avoid interleaving buffered and unbuffered.
# After this, `set -x` will synchronize output with the upcoming DEBUG trap.
exec 2>&1

set -x  # print out commands before running them
set -x  # (even print itself out!)
set -e  # exit when any command fails
set -u  # exit when attempting to expand any unset variable

# For every command, this DEBUG trap will:
# - First print a newline and change color to light purple.
# - Then print the Bash command with source and line number.
# - Finally reset color and print newline.
#
# We wrap the printf statement inside curly brackets to allow redirecting STDERR
#   output (i.e. the `set -x` mode) into oblivion.
# Thus, the `{ printf ...; } 2>&-` comand itself will not show up in STDERR.
#
# Inspired by: https://www.shell-tips.com/bash/debug-script/
# Another reference: https://tldp.org/LDP/abs/html/debugging.html
trap "$(cat <<'END_OF_DEBUG_TRAP'
{
  printf '\n\e[1;35m%s\e[0m\n' "[${FUNCNAME[0]:-main}:${LINENO}] ${BASH_COMMAND}";
} 2>&-
END_OF_DEBUG_TRAP
)" DEBUG

# ------------------------------------------------------------------------------
# Declaring constants and helpers
# ------------------------------------------------------------------------------

# Mostly relative files and folders used by the script.
readonly BUILD_DIR=build
readonly SOURCE_DIR=src
readonly TEX_INCLUDES_CONFIG_FILE=config/tex_includes.txt
readonly TEX_EXCLUDES_CONFIG_FILE=config/tex_excludes.txt

function usage()
{
  cat <<END_OF_USAGE
Usage:
  $0 [--small_subset]

Available Options:
  --small_subset
    Picks a small subset of Tex files to compile. Much quicker!
    Useful for debugging purposes. Not intended for production.
END_OF_USAGE
}

function die_with_usage()
{
  printf "%s\n\n" "$*" >&2
  usage >&2
  # An exit status of 2 indicates misuse of shell builtins, according to:
  #   https://tldp.org/LDP/abs/html/exitcodes.html
  exit 2
}

# ------------------------------------------------------------------------------
# Parsing command-line arguments
# ------------------------------------------------------------------------------

# Bash doesn't have native booleans. Integers are an alternative approach.
# Non-zero integers are truthy. Only zero is falsey.
# For more examples of integers used as booleans, see:
#   https://gitlab.com/matt-n-mo/tex-to-pdf/-/snippets/2475604
declare -i BOOL_OPTION_SMALL_SUBSET=0

# `getopts` is a Bash builtin. It manages `OPTIND` and `OPTARG` variables.
# It's second argument (in this case `OPT`) also becomes a Bash variable.
# Oddly, the first argument of `getopts` cannot begin with a '-'. Yet, it
#   can begin with a space, which should not affect anything.
while getopts ' -:' OPT
do
  # Support long options.
  # Inspired by: https://stackoverflow.com/a/28466267/519360
  if [[ "${OPT}" == "-" ]]
  then
    OPT="${OPTARG%%=*}"  # extract long option name
    declare -i HAS_LONG_ARG=$(( ${#OPTARG} > ${#OPT} ))
    if (( HAS_LONG_ARG ))
    then
      OPTARG="${OPTARG#$OPT=}"
    else
      OPTARG=""
    fi
  else
    unset HAS_LONG_ARG
  fi

  case "${OPT}" in
    small_subset )
      (( ! HAS_LONG_ARG )) || die_with_usage "Argument not allowed for --${OPT}"
      BOOL_OPTION_SMALL_SUBSET=1
      ;;
    [?] )  # OPT is a literal "?" character that `getopts` gives us
      # In this case, a diagnostic error is already reported by `getopts`.
      die_with_usage "Unrecognized short option; see diagnostic error above"
      ;;
    * )  # OPT came from our logic above
      die_with_usage "Unrecognized long option --${OPT}"
      ;;
  esac
done

shift $((OPTIND-1))  # remove parsed options and args from $@ list

(( $# == 0 )) || die_with_usage "Positional arguments are not allowed"

# ------------------------------------------------------------------------------
# Reading configuration
# ------------------------------------------------------------------------------

# Before doing anything, first populate Bash arrays from configuration.
# When using `readarray`, the default delimiter is a newline $"\n".
# Note that the "-t" option MUST occur before the array name, or else
#   it won't discard the newline from each array element
readarray -t TEX_INCLUDES <"${TEX_INCLUDES_CONFIG_FILE}"
readarray -t TEX_EXCLUDES <"${TEX_EXCLUDES_CONFIG_FILE}"
readonly -a TEX_INCLUDES TEX_EXCLUDES

# ------------------------------------------------------------------------------
# Finding all LaTeX files
# ------------------------------------------------------------------------------

# `find` arguments will be assembled into this array
declare -a TEX_FIND_ARGUMENTS=()

# Tell `find` to search each of these directories
for DIR in "${TEX_INCLUDES[@]}"
do
  echo "Going to look for .tex files matching ${DIR}"
  TEX_FIND_ARGUMENTS+=( "${SOURCE_DIR}/${DIR}" )
done

# Tell `find` to exclude all of these directories
TEX_FIND_ARGUMENTS+=( -name .git -type d -prune -o )
for PATTERN in "${TEX_EXCLUDES[@]}"
do
  echo "Avoiding .tex files/directories that start with ${PATTERN}"
  TEX_FIND_ARGUMENTS+=( -path "${SOURCE_DIR}/${PATTERN}" -prune -o )
done

# Tell `find` to print out all Tex files it found.
# Separate each file with a null terminator character.
TEX_FIND_ARGUMENTS+=( -name "*.tex" -print0 )

# Note that `readarray` is a Bash builtin that populates a variable from the
#   contents of stdin.
# When an empty string is supplied as an argument to the "-d" option,
#   it splits the input on the null terminator character.
declare -a TEX_FILES
readarray -td '' TEX_FILES < <(find "${TEX_FIND_ARGUMENTS[@]}")
echo "Found ${#TEX_FILES[@]} file(s) to compile into PDFs"

if (( BOOL_OPTION_SMALL_SUBSET ))
then
  TEX_FILES=(
    "${SOURCE_DIR}/grad-school-writings/class-notes/quantum-groups.tex"
    "${SOURCE_DIR}/grad-school-writings/class-notes/springer-theory-notes.tex"
  )
  echo "Just kidding! We're starting off with ${#TEX_FILES[@]} dummy file(s)"
fi

# ------------------------------------------------------------------------------
# Compiling LaTeX sources into PDFs
# ------------------------------------------------------------------------------

echo "Starting the latexmk command!"
LATEXMK_COMMAND_OPTIONS=(
  # We're making PDFs, after all :)
  -pdf

  # Make imports relative to each source file, not the current working directory
  -cd

  # Batch mode is like nonstop mode, except much less verbose!
  -interaction=batchmode
)
latexmk "${LATEXMK_COMMAND_OPTIONS[@]}" "${TEX_FILES[@]}"
echo "Finished the latexmk command!"

# Print all PDF files as a sanity check
echo "The ${SOURCE_DIR}/ now has these PDFs:"
find "${SOURCE_DIR}" -name "*.pdf" -print

# ------------------------------------------------------------------------------
# Exporting PDFs as artifacts
# ------------------------------------------------------------------------------

# Move the LaTeX PDFs to the appropriate destination.
echo "Moving the PDF files into destination folder: ${BUILD_DIR}"
mkdir -p "${BUILD_DIR}"
for EACH_TEX_FILE in "${TEX_FILES[@]}"
do
  # The % is used to strip a suffix in a Bash parameter substitution.
  # The following turns "src/path/to/foo.tex" into "src/path/to/foo.pdf"
  declare SRC_PDF_FILE="${EACH_TEX_FILE%.tex}.pdf"
  echo "Source PDF file name: ${SRC_PDF_FILE}"

  # The # is used to strip a prefix in a Bash parameter substitution.
  # The following turns "src/path/to/foo.pdf" into "build/path/to/foo.pdf"
  declare DESTINATION_PDF_FILE="${BUILD_DIR}/${SRC_PDF_FILE#${SOURCE_DIR}/}"
  echo "Destination PDF file name: ${DESTINATION_PDF_FILE}"

  # Move an individual file!
  declare DESTINATION_PDF_DIR=$(dirname ${DESTINATION_PDF_FILE})
  mkdir -p "${DESTINATION_PDF_DIR}"
  echo "Destination PDF directory: ${DESTINATION_PDF_DIR}"
  mv -- "${SRC_PDF_FILE}" "${DESTINATION_PDF_FILE}"
done
