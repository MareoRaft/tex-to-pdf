Built files (pdfs) will get populated here.

Normally we would not commit files into this folder, but I may commit some so I can experiment with file export integrations.
